<?php

namespace Database\Factories;

use App\Models\Transaction;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Faker;

class TransactionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Transaction::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'code' => strtoupper(Str::random(5)),
            'customer_name' => ucwords($this->faker->name),
            'amount' => $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 500000, $max = 2000000),
            'status' => 1,
            'created_at' => $this->faker->unique()->dateTimeBetween($startDate = '-3 months', $endDate = 'now', $timezone = null)
        ];
    }
}
