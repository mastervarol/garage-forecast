<?php

namespace Database\Factories;

use App\Models\ItemTransaction;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ItemTransactionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ItemTransaction::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'transaction_id' => $this->faker->unique()->numberBetween($min = 1, $max = 100),
            'product_id' => $this->faker->numberBetween($min = 1, $max = 5),
            'qty' => $this->faker->numberBetween($min = 1, $max = 3),
            'price' => $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 100000, $max = 1000000),
            'grand_total' => $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 100000, $max = 1000000),
            'created_at' => $this->faker->unique()->dateTimeBetween($startDate = '-3 months', $endDate = 'now', $timezone = null)
        ];
    }
}
