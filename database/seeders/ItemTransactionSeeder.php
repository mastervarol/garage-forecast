<?php

namespace Database\Seeders;

use App\Models\ItemTransaction;
use Illuminate\Database\Seeder;

class ItemTransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        ItemTransaction::truncate();
        ItemTransaction::factory(100)->create();
    }
}
