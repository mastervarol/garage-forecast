<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\ItemTransaction;
use Illuminate\Support\Str;

class ApiController extends Controller
{
    //
    public function productList()
    {
        $data = Product::all();

        return response()->json($data);
    }

    public function newTransaction(Request $request)
    {
        $data = new Transaction();

        $data->code = strtoupper(Str::random(5));
        $data->customer_name = ucwords($request->name);
        $data->status = 0;

        $data->save();

        return response()->json($data);
    }

    public function cartList(Request $request)
    {
        $cname = $request->cname;
        $trx = Transaction::where('customer_name', $cname)
            ->where('status', 0)
            ->first();
        $data = ItemTransaction::with('product')
            ->where('transaction_id', $trx->id)->get();

        return response()->json($data);
    }

    public function addCart(Request $request)
    {
        $pid = $request->pid;
        $cname = $request->cname;
        $data = Transaction::with('items')
            ->where('customer_name', $cname)
            ->where('status', 0)
            ->first();
        $message = '';

        // Check if trx exist
        if ($data) {
            $message = 'Initiate checking item';
            // $items = ItemTransaction::where('transaction_id', $data->id)
            //     ->get();
            // Check if trx has items already
            $message = 'Check if trx has items already';
            if (count($data->items) > 0) {
                // Get xurrent trx items 
                $message = 'Get current trx items';
                foreach ($data->items as $item) {
                    // Check if new product exist on cart 
                    $message = 'Item ID : ' . $item->id . '; PID : ' . $pid;
                    if ($item->id == $pid) {
                        // Update qty if item exist
                        $message = 'Update existing item';
                        $item->pivot->qty = $item->pivot->qty + 1;
                        $item->pivot->grand_total = $item->pivot->price * $item->pivot->qty;

                        $item->save();
                        return true;
                    } else {
                        $product = Product::findOrfail($pid);
                        $message = 'Insert new record of item';

                        // Insert new record if item doesnt exist
                        $newItem = new ItemTransaction();
                        $newItem->transaction_id = $data->id;
                        $newItem->product_id = $pid;
                        $newItem->qty = 1;
                        $newItem->price = $product->price;
                        $newItem->grand_total = $newItem->price * $newItem->qty;

                        $newItem->save();
                        return true;
                    }
                }
            } else {
                $product = Product::findOrfail($pid);
                $message = 'Insert first record if item doesnt exist';

                // Insert new record if item doesnt exist
                $newItem = new ItemTransaction();
                $newItem->transaction_id = $data->id;
                $newItem->product_id = $pid;
                $newItem->qty = 1;
                $newItem->price = $product->price;
                $newItem->grand_total = $newItem->price * $newItem->qty;

                $newItem->save();
            }
        }

        return response()->json([
            'data' => $data,
            'message' => $message
        ]);
    }
}
