<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    //
    public function index()
    {
        return view('login');
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $rules = [
            'email'    => 'required|email',
            'password' => 'required|alphaNum|min:3'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('login')
                ->withErrors($validator)
                ->withInput();
        }

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'is_active' => 1])) {
            // Authentication passed...
            return redirect()->intended('/');
        } else {
            Session::flash('message', 'Credentials not found!');

            return redirect()->route('login')->withInput();
        }
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('login');
    }
}
