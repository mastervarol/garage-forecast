<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\Forecast;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\Foreach_;

class DashboardController extends Controller
{
    //
    public function index()
    {   
        $today = Carbon::now()->month;

        $products = Product::count();
        $categories = Category::count();
        $trx = Transaction::count();
        $trx_month = Transaction::whereMonth('created_at', $today)->count();
        $forecast = Forecast::take(5)->get();
        // return $forecast;
        return view('dashboard')->with([
            'products' => $products,
            'categories' => $categories,
            'trx' => $trx,
            'trx_month' => $trx_month,
            'forecast' => $forecast
        ]);
    }
}
