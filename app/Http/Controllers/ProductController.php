<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Product::whereHas('category')
            ->orderByDesc('created_at')
            ->get();

        return view('product.index')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::all();

        return view('product.create')->with([
            'categories' => $categories
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = [
            'category' => 'required',
            'image' => 'required',
            'name' => 'required',
            'price' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $data = new Product();
        $message = 'New product has been created successfully';
        $alert = 'success';

        $this->insertData($request, $data);
        $data->code = strtoupper(Str::random(5));
        $data->save();

        Session::flash('message', $message);
        Session::flash('alert', $alert);

        return redirect()->route('product');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Product::findOrfail($id);
        $categories = Category::all();
        // dd($categories);
        return view('product.edit')->with([
            'data' => $data,
            'categories' => $categories
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rules = [
            'category' => 'required',
            'name' => 'required',
            'price' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $data = Product::findOrfail($id);
        $message = 'Product has been updated successfully';
        $alert = 'success';

        $this->insertData($request, $data);

        Session::flash('message', $message);
        Session::flash('alert', $alert);

        return redirect()->route('product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Product::findOrfail($id)->delete();
        $message = 'User has been deleted successfully';
        $alert = 'danger';

        Session::flash('message', $message);
        Session::flash('alert', $alert);

        return redirect()->back();
    }

    private function insertData($request, $data)
    {
        $data->category()->associate($request->category);
        $data->name = $request->name;
        $data->price = $request->price;

        if($request->hasFile('image')){
            $path = public_path('/images/product/');
            if(!File::isDirectory($path)){
                File::makeDirectory($path, 0777, true, true);
            }
            $image = md5(Str::random(64)). '.' .$request->file('image')->getClientOriginalExtension();

            $data->image = $image;

            Image::make($request->image)->save($path.$image);
        }

        $data->save();
    }
}
