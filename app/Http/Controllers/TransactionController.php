<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Transaction;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class TransactionController extends Controller
{
    //
    public function index()
    {
        $data = Transaction::orderByDesc('id')->get();

        return view('transaction.index')->with([
            'data' => $data
        ]);
    }

    public function create()
    {
        $data = Product::whereHas('category')->get();

        return view('transaction.create')->with([
            'data' => $data
        ]);
    }

    public function checkoutForm(Request $request, $id)
    {
        $data = Transaction::findOrfail($id);

        return view('transaction.checkout')->with([
            'data' => $data
        ]);
    }

    public function checkout(Request $request)
    {
        $trx_id = $request->trx_id;
        $trx = Transaction::findOrfail($trx_id);
        $arr = 0;
        $grand_total = 0;

        foreach($trx->items as $item){
            
            $trx->items()->updateExistingPivot($request->item_id[$arr], [
                'qty' => $request->item_qty[$arr],
                'grand_total' => $item->pivot->price * $request->item_qty[$arr]
            ]);

            $grand_total = $grand_total + $item->pivot->price * $request->item_qty[$arr];
            $arr += 1;
        }
        
        $trx->status = 1;
        $trx->amount = $grand_total;
        $trx->save();

        Session::flash('message', 'Checkout Complated');

        return redirect()->route('transaction.confirmation', ['id' => $request->trx_id]);
    }

    public function confirmation($id)
    {
        $data = Transaction::findOrfail($id);

        return view('transaction.confirmation')->with([
            'data' => $data
        ]);
    }

    public function detail($id)
    {
        $data = Transaction::findOrfail($id);

        return view('transaction.detail')->with([
            'data' => $data
        ]);
    }
}
