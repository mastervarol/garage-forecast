<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Forecast;
use App\Models\ItemTransaction;
use App\Models\Product;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class ForecastController extends Controller
{
    //
    public function index()
    {
        $data = Forecast::orderByDesc('created_at')->get();

        return view('forecast.index')->with([
            'data' => $data
        ]);
    }

    public function show($id)
    {
        $data = Forecast::findOrfail($id);

        return view('forecast.show')->with([
            'data' => $data
        ]);
    }

    public function forecast($user)
    {
        $start = Carbon::now()->subMonths(2)->format('Y-m-d');
        $end = Carbon::now()->format('Y-m-d');
        $user  = $user;
        $link = '';

        $s = Carbon::parse($start);

        $products = Product::with(['transactions' => function ($q) use ($start, $end) {
            $q->whereDate('transactions.created_at', '>=', $start);
            $q->whereDate('transactions.created_at', '<=', $end);
        }])
            ->whereHas('transactions', function ($q) use ($start, $end) {
                $q->whereDate('transactions.created_at', '>=', $start);
                $q->whereDate('transactions.created_at', '<=', $end);
            })->get();
        // return response()->json($products);
        if ($products) {
            foreach ($products as &$product) {
                $m = $s->month;
                $m1 = $s->month + 1;
                $m2 = $s->month + 2;

                $product->{'m-' . $m} = 0;
                $product->{'m-' . $m1} = 0;
                $product->{'m-' . $m2} = 0;
                $product->total_qty = 0;

                foreach ($product->transactions as $trans) {
                    $product->total_qty = $product->total_qty + $trans->pivot->qty;

                    $product->{'m-' . $trans->created_at->month} = $product->{'m-' . $trans->created_at->month} + $trans->pivot->qty;
                }

                unset($product->transactions);
            }
            unset($product);

            $month_forecast = new Carbon('first day of next month');
            $forecast = new Forecast();
            $forecast->code = strtoupper(Str::random(5));
            $forecast->month_forecast = $month_forecast;
            $forecast->processedBy()->associate($user);

            $forecast->save();
            $link = "{{ url('forecast/show/'. $forecast->id .) }}";

            foreach ($products as $data) {
                $forecast->items()->attach($data->id, [
                    'result' => $data->total_qty
                ]);
            }

            return response()->json([$products, $link]);
        } else {
            return response('No transactions', 204);
        }
    }
}
