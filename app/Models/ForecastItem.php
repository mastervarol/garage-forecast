<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ForecastItem extends Model
{
    use HasFactory;

    public function forecast()
    {
        return $this->belongsTo('App\Models\Forecast');
    }
}
