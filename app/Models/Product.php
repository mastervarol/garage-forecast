<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function transactions()
    {
        return $this->belongsToMany('App\Models\Transaction', 'item_transactions')->withPivot('qty', 'price', 'grand_total');
    }

    public function createdAt()
    {
        return \Carbon\Carbon::parse($this->created_at)->format('d M Y');
    }

    public function price()
    {
        return 'IDR '.number_format($this->price, 2, ',', '.');
    }
}
