<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
    ];

    public function items()
    {
        return $this->belongsToMany('App\Models\Product', 'item_transactions')->withPivot('qty', 'price', 'grand_total');
    }

    public function price()
    {
        return 'IDR '.number_format($this->amount, 2, ',', '.');
    }

    public function createdAt()
    {
        return \Carbon\Carbon::parse($this->created_at)->format('d M Y');
    }
}
