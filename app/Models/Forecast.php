<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Forecast extends Model
{
    use HasFactory;

    public function items()
    {
        return $this->belongsToMany('App\Models\Product', 'forecast_items')->withPivot('result');
    }

    public function monthForecast()
    {
        return Carbon::parse($this->month_forecast)->format('M Y');
    }

    public function processedBy()
    {
        return $this->belongsTo('App\Models\User', 'processed_by', 'id');
    }

    public function createdAt()
    {
        return \Carbon\Carbon::parse($this->created_at)->format('d M Y');
    }
}
