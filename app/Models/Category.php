<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function transactions()
    {
        return $this->hasManyThrough('App\Models\Product', 'App\Models\Transaction');
    }

    public function createdAt()
    {
        return \Carbon\Carbon::parse($this->created_at)->format('d M Y');
    }
}
