<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItemTransaction extends Model
{
    use HasFactory;

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function price()
    {
        return 'IDR '.number_format($this->price, 2, ',', '.');
    }

    public function grandTotal()
    {
        $total = $this->price * $this->qty;
        return 'IDR '.number_format($total, 2, ',', '.');
    }
}
