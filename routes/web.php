<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ForecastController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LoginController;
use Illuminate\Auth\Events\Login;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Login
Route::get('login', [LoginController::class, 'index'])->name('login');
Route::post('authenticate', [LoginController::class, 'authenticate'])->name('authenticate');
Route::get('logout', [LoginController::class, 'logout'])->name('logout');
Route::group(['prefix' => '/', 'middleware' => 'auth'], function () {
    // Dashboard
    Route::get('/', [DashboardController::class, 'index'])->name('home');
    // User
    Route::resource('user', UserController::class, [
        'names' => [
            'as' => 'prefix',
            'index' => 'user',
            'create' => 'user.create',
            'store' => 'user.store',
            'edit' => 'user.edit',
            'update' => 'user.update',
            'destroy' => 'user.delete'
        ]
    ]);
    // Product
    Route::resource('product', ProductController::class, [
        'names' => [
            'as' => 'prefix',
            'index' => 'product',
            'create' => 'product.create',
            'store' => 'product.store',
            'edit' => 'product.edit',
            'update' => 'product.update',
            'destroy' => 'product.delete'
        ]
    ]);
    // Category
    Route::resource('category', CategoryController::class, [
        'names' => [
            'as' => 'prefix',
            'index' => 'category',
            'create' => 'category.create',
            'store' => 'category.store',
            'edit' => 'category.edit',
            'update' => 'category.update',
            'destroy' => 'category.delete'
        ]
    ]);
    // Transaction
    Route::get('transaction', [TransactionController::class, 'index'])->name('transaction');
    Route::get('transaction/create', [TransactionController::class, 'create'])->name('transaction.create');
    Route::post('transaction/checkout', [TransactionController::class, 'checkout'])->name('transaction.checkout');
    Route::get('transaction/confirmation/{id}', [TransactionController::class, 'confirmation'])->name('transaction.confirmation');
    Route::get('transaction/detail/{id}', [TransactionController::class, 'detail'])->name('transaction.detail');

    // Transaction
    Route::get('forecast', [ForecastController::class, 'index'])->name('forecast');
    Route::get('forecast/show/{id}', [ForecastController::class, 'show'])->name('forecast.show');
});
