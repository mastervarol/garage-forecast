<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\ForecastController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
// Transaction API
Route::post('newTransaction', [ApiController::class, 'newTransaction'])->name('api.newTransaction');
Route::get('productList', [ApiController::class, 'productList'])->name('api.productList');
Route::get('cartList', [ApiController::class, 'cartList'])->name('api.cartList');
Route::post('addCart', [ApiController::class, 'addCart'])->name('api.addCart');
// Forecasting API
Route::get('forecast/{user}', [ForecastController::class, 'forecast'])->name('api.forecast');
