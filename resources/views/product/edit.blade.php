@extends('template')
@push('additional_css')

@endpush
@push('additional_js')

@endpush
@push('additional_script')
    <script>

    </script>
@endpush
@push('content_header')
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0"> Product</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home', []) }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('product', []) }}">Category</a></li>
                        <li class="breadcrumb-item active">Edit</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
@endpush
@section('content')
    <section class="content">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-12">
                    @if ($errors->any())
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-exclamation-triangle"></i> Alert!</h5>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Product Form</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="{{ route('product.update', ['product' => $data->id]) }}" method="POST"
                            enctype="multipart/form-data">
                            @method('PUT')
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="category">Select Category</label>
                                    <select class="form-control" name="category" id="category">
                                        @foreach ($categories as $item)
                                            @if ($item->id == $data->id)
                                                <option value="{{ $item->id }}" selected hidden>{{ $item->name }}
                                                </option>
                                            @else
                                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Enter name"
                                        value="{{ old('name', $data->name) }}" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <label for="name">Price</label>
                                    <input type="number" class="form-control" name="price" id="price"
                                        placeholder="Enter price" value="{{ old('price', $data->price) }}"
                                        autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <label for="image">Product Image</label> <br>
                                    <img class="profile-user-img img-fluid img-circle"
                                        src="{{ $item->image ? asset('/images/product/' . $item->image) : asset('/dist/img/default-150x150.png') }}"
                                        alt="User profile picture">
                                    <div class="input-group mt-3">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="image" id="image">
                                            <label class="custom-file-label" for="exampleInputFile">Choose image</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection
