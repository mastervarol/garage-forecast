@extends('template')
@push('additional_css')
    
@endpush
@push('additional_js')
    <script src="{{ url('https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js', []) }}"></script>
    <script src="{{ url('https://cdn.jsdelivr.net/npm/sweetalert2@10', []) }}"></script>
@endpush
@push('additional_script')
<script>
    $(document).ready(function() {
        console.log('Document ready!');
        console.log('Customer : ', localStorage.getItem('customer'));
        // localStorage.removeItem('customer');
        // loadData();

        var customer = localStorage.getItem('customer');

        if(!customer){
            $('#modal-default').modal({
                backdrop: 'static',
                keyboard: false
            });
        }else{
            cartList();
            $('#cust_name').html(' - ' + customer)
        }
    });

    async function loadData()
    {
        console.log('Fechting product..');

        try {
            const response = await axios.get('{{ route("api.productList") }}')
            .then(function (response) {
                // handle success
                console.log(response.data);

                data = response.data;
                var container = $('#product-list');
                console.log(container);

                for (const item of data){
                    console.log(item);
                    container.append('<div class="col-md-3">' +
                    '<div class="card card-primary card-outline">' +
                        '<div class="card-body box-profile">' +
                            '<div class="text-center">' +
                            '<img class="profile-user-img img-fluid img-circle" src="http://localhost/garage-forecast/public/images/product/'+ item.image +'" alt="User profile picture" style="height: 100px; width: 100px;">' +
                            '</div>' +
                            '<h3 class="profile-username text-center">'+ item.name +'</h3>' +
                            '<p class="text-muted text-center">IDR '+ item.price +'</p>' +
                            '<a href="#" class="btn btn-primary btn-block">' +
                                '<b><i class="fa fa-cart-plus"></i> Add To Cart</b>' +
                            '</a>' +
                            '</div>' +
                        '</div>' +
                    '</div>');
                }
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .then(function () {
                // always executed
            });
        } catch (error) {
            console.error(error);
        }
    }

    function newTransaction()
    {
        var name = $('#customer_name').val();
        // return alert(name);
        axios.post('{{ route('api.newTransaction') }}',{
            name: name
        })
        .then(function(response){
            $('#modal-default').modal('toggle');
            console.log(response);
            localStorage.setItem('customer', name);

            Swal.fire({
                title: 'Cool!',
                text: 'New transaction has been made. Put some item now.',
                icon: 'success',
                confirmButtonText: 'Cool'
            })

            $('#cust_name').html(' - ' + localStorage.getItem('customer'))
        }).catch(function(error){
            Swal.fire({
                title: 'Error!',
                text: error,
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        });
    }

    function cartList()
    {
        // Make a request for a user with a given ID
        axios.get('{{ route('api.cartList') }}',{
            params: {
                cname : localStorage.getItem('customer')
            }
        })
        .then(function (response) {
            // handle success
            console.log('cartList response => ', response);
            data = response.data;
            container = $('#tbody');
            container.html('');

            for (const item of data){
                console.log('Item list => ', item);
                container.append('<tr align="center">'+
                    '<td>'+
                        '<input type="hidden" name="trx_id" value="'+ item.transaction_id +'">'+
                        '<input type="hidden" name="item_id[]" id="item_name" value="'+ item.product.id +'">'+
                        '<span id="item-name">'+ item.product.name +'</span>'+
                    '</td>'+
                    '<td>'+
                        '<input type="number" class="form-control" style="max-width: 70px;" min="1" value="'+ item.qty +'" name="item_qty[]" id="item_qty">'+
                    '</td>'+
                '</tr>');
            }
        })
        .catch(function (error) {
            // handle error
            console.log('cartList Error => ', error);
        })
        .then(function () {
            // always executed
        });
    }

    function addCart(pid)
    {
        if(!localStorage.getItem('customer')){
            return $('#modal-default').modal({
                backdrop: 'static',
                keyboard: false
            });
        }
        // return alert(localStorage.getItem('customer'));
        axios.post('{{ route('api.addCart') }}', {
            pid: pid,
            cname: localStorage.getItem('customer')
        })
        .then(function (response) {
            console.log('addCart response => ', response);
            Swal.fire({
                title: 'Cool!',
                text: 'Item has added to cart.',
                icon: 'success',
                confirmButtonText: 'Cool'
            });

            cartList();
        })
        .catch(function (error) {
            console.log(error);
            Swal.fire({
                title: 'Opps!',
                text: error,
                icon: 'error',
                confirmButtonText: 'Cool'
            });
        });
    }

    function confirmCheckout()
    {
        if(confirm('Confirm checkout?')){
            localStorage.removeItem('customer');
        }else{
            return false;
        }
    }
</script>
@endpush
@push('content_header')
<div class="content-header">
    <div class="container">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0"> Transaction</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('home', []) }}">Home</a></li>
            <li class="breadcrumb-item active">Transaction</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
@endpush
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9">
                @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-exclamation-triangle"></i> Alert!</h5>
                    {{ Session::get('message') }}
                </div>
                @endif
                <div class="row" id="product-list">
                    @foreach ($data as $item)
                        <div class="col-md-3">
                            <div class="card card-primary card-outline">
                            <div class="card-body box-profile">
                                <div class="text-center">
                                <img class="profile-user-img img-fluid img-circle" src="{{ $item->image ? asset('/images/product/'. $item->image) : asset('/dist/img/default-150x150.png') }}" alt="User profile picture" style="height: 100px; width: 100px;">
                                </div>
                                <h3 class="profile-username text-center">{{ $item->name }}</h3>
                                <p class="text-center text-success" style="margin-top: -10px;">{{ $item->category->name }}</p>
                                <p class="text-muted text-center">{{ $item->price() }}</p>
                                <a href="#" class="btn btn-primary btn-block" onclick="return addCart({{ $item->id }})">
                                    <b><i class="fa fa-cart-plus"></i> Add To Cart</b>
                                </a>
                            </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-3">
                <div class="card card-info">
                    <form action="{{ route('transaction.checkout', []) }}" method="POST">
                        @method('POST')
                        @csrf
                        <div class="card-header">
                        <h3 class="card-title">Cart <span id="cust_name"></span></h3>
                        </div>
                        <div class="card-body p-0">
                        <table class="table">
                            <thead>
                            <tr align="center">
                                <th>Product</th>
                                <th>Qty</th>
                            </tr>
                            </thead>
                            <tbody id="tbody">
                            {{-- Data by cartList --}}
                            </tbody>
                        </table>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <input type="submit" class="btn btn-primary btn-block" value="Checkout" onclick="return confirmCheckout();">
                                {{-- <b><i class="fa fa-shopping-basket"></i> Checkout</b> --}}
                            </input>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">New Transaction</h4>
          {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> --}}
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label for="exampleInputEmail1">Customer Name</label>
                <input type="text" class="form-control" id="customer_name" name="name" placeholder="Enter customer name.." autocomplete="false">
            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="return newTransaction();">Create</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection