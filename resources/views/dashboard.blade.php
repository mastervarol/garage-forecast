@extends('template')
@push('additional_css')
    
@endpush
@push('additional_js')
    
@endpush
@push('additional_script')
    
@endpush
@push('content_header')
<div class="content-header">
    <div class="container">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0"> Home</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
           
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
@endpush
@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-12">
              <div class="info-box">
                <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>
  
                <div class="info-box-content">
                  <span class="info-box-text">Active Products</span>
                  <span class="info-box-number">{{ $products }}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-12">
              <div class="info-box">
                <span class="info-box-icon bg-success"><i class="far fa-flag"></i></span>
  
                <div class="info-box-content">
                  <span class="info-box-text">Active Product Categories</span>
                  <span class="info-box-number">{{ $categories }}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-12">
              <div class="info-box">
                <span class="info-box-icon bg-warning"><i class="far fa-copy"></i></span>
  
                <div class="info-box-content">
                  <span class="info-box-text">Total Transactions</span>
                  <span class="info-box-number">{{ $trx }}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-12">
              <div class="info-box">
                <span class="info-box-icon bg-danger"><i class="far fa-star"></i></span>
  
                <div class="info-box-content">
                  <span class="info-box-text">Transaction This Month</span>
                  <span class="info-box-number">{{ $trx_month }}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
          </div>
        <div class="row">
        <!-- /.col-md-6 -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">Latest Forecast</h5>
                    </div>
                    <div class="card-body">
                        <div class="timeline">
                            <!-- timeline item -->
                            @foreach ($forecast as $item)
                                <div>
                                    <i class="fas fa-user bg-green"></i>
                                    <div class="timeline-item">
                                        <span class="time"><i class="fas fa-clock"></i> {{ $item->created_at->diffForHumans() }}</span>
                                        <h3 class="timeline-header no-border">{{ $item->processedBy->name }} making new forecast ID:<a href="{{ route('forecast.show', ['id'=>$item->id]) }}"> {{ $item->code }}</a></h3>
                                    </div>
                                </div>
                            @endforeach
                            <!-- END timeline item -->
                        </div>
                    </div>
                </div>
            </div>
        <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
@endsection