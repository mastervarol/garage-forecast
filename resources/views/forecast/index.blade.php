@extends('template')
@push('additional_css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush
@push('additional_js')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('/plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('/plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    {{-- Axios & Swall 2 --}}
    <script src="{{ url('https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js', []) }}"></script>
    <script src="{{ url('https://cdn.jsdelivr.net/npm/sweetalert2@10', []) }}"></script>
@endpush
@push('additional_script')
    <script>
        $(document).ready(function() {
            $("#example1").DataTable({
                "ordering": false
            });
            $('#state-progress').css('display', 'none');
            $('#state-finish').css('display', 'none');
            $('#state-noData').css('display', 'none');
            $('#state-error').css('display', 'none');
            $('#btn-left').html('Cancel');
        });
        // $('#modal-default').modal({
        //     backdrop: 'static',
        //     keyboard: false
        // });

        function newForecast() {
            var sOpen = $('#state-open');
            var sProgress = $('#state-progress');
            var sFinish = $('#state-finish');
            var btnGo = $('#btn-go');
            var sNoData = $('#state-noData');
            var sError = $('#state-error');
            var errorMessage = $('#error-message');
            var link = $('#link');
            var btnLeft = $('#btn-left');

            sOpen.css('display', 'none');
            sProgress.css('display', 'block');
            sFinish.css('display', 'none');
            sError.css('display', 'none');
            errorMessage.css('display', 'none');
            btnLeft.html('Cancel');

            axios.get('{{ route('api.forecast', ['user' => Auth::user()->id]) }}')
                .then(function(response) {
                    // handle success
                    console.log(response);
                    if (response.data.length < 1) {
                        sProgress.css('display', 'none');
                        sNoData.css('display', 'block');
                    } else {
                        sOpen.css('display', 'none');
                        sProgress.css('display', 'none');
                        sFinish.css('display', 'block');
                        btnGo.css('display', 'none');
                        link.attr('href', '{{ route('forecast.show', ['id' => '+ response.data[1] +']) }}');
                        $('#btn-left').html('Ok');
                        $('#btn-left').click(function() {
                            location.reload();
                        });
                    }
                })
                .catch(function(error) {
                    // handle error
                    console.log(error);
                    sProgress.css('display', 'none');
                    sError.css('display', 'block');
                    errorMessage.css('display', 'block');
                    errorMessage.html(error);
                })
                .then(function() {
                    // always executed
                    // sOpen.css('display', 'none');
                    // sProgress.css('display', 'none');
                    // sFinish.css('display', 'block');
                    // btnGo.css('display', 'none');
                });
        }

    </script>
@endpush
@push('content_header')
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0"> Forecast Log</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home', []) }}">Home</a></li>
                        <li class="breadcrumb-item active">Forecast</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
@endpush
@section('content')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    @if (Session::has('message'))
                        <div class="alert alert-{{ Session::get('alert') }} alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-exclamation-triangle"></i> Alert!</h5>
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3 class="card-title" style="margin-top: 35px;">
                                        {{-- User List --}}
                                    </h3>
                                </div>
                                <div class="col-md-6 text-right">
                                    <a href="#" class="btn btn-app mt-3" data-toggle="modal" data-target="#modal-default">
                                        <i class="fas fa-plus"></i> Add New
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-hover">
                                <thead>
                                    <tr align="center">
                                        <th>Code</th>
                                        <th>Month</th>
                                        <th>Created At</th>
                                        <th style="width: 10%">#</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $item)
                                        <tr align="center">
                                            <td>
                                                {{ $item->code }}
                                            </td>
                                            <td>
                                                {{ $item->monthForecast() }}
                                            </td>
                                            <td>
                                                Date Forecast : {{ $item->createdAt() }} <br>
                                                Processed by : {{ $item->processedBy->name }}
                                            </td>
                                            <td>
                                                <a href="{{ route('forecast.show', ['id' => $item->id]) }}"
                                                    class="btn btn-default btn-block">
                                                    <div class="fa fa-eye" title="Detail"></div>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr align="center">
                                        <th>Code</th>
                                        <th>Month</th>
                                        <th>Created At</th>
                                        <th style="width: 10%">#</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">New Forecast</h4>
                    {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> --}}
                </div>
                <div class="modal-body text-center">
                    <span id="state-open">
                        <h5>Confirm Forecasting for the upcoming month?</h5>
                    </span>
                    <span id="state-progress">
                        <h5>Forecasting on progress, please wait.</h5>
                        <div class="bg-light disabled color-palette"><span>Do not close the proccess!</span></div>
                        <i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>
                    </span>
                    <span id="state-finish">
                        <h5>Forecast done <i class="fa fa-check" aria-hidden="true"></i></h5>
                        <div class="bg-light disabled color-palette"><a href="#" id="link" target="_blank">Click here to
                                check!</a></div>
                    </span>
                    <span id="state-error">
                        <h5>Forecast error <i class="fa fa-ban" aria-hidden="true"></i></h5>
                        <div class="bg-light disabled color-palette" id="error-message">Click here to check!</div>
                    </span>
                    <span id="state-noData">
                        <h5>Forecast Failed <i class="fa fa-ban" aria-hidden="true"></i></h5>
                        <div class="bg-light disabled color-palette"><span>No transaction data!</span></div>
                    </span>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" id="btn-left" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="btn-go" onclick="return newForecast();">Go!</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endsection
